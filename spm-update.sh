#!/bin/sh
set -e

cmake .
mv libtiff/tif_config.h libtiff/include-SPM/tif_config.h
mv libtiff/tiffconf.h libtiff/include-SPM/tiffconf.h
mv libtiff/tiffvers.h libtiff/include-SPM/tiffvers.h

sed -i '' -e 's|#define OJPEG_SUPPORT 1|/* #define OJPEG_SUPPORT 1 */|g' libtiff/include-SPM/tiffconf.h
sed -i '' -e 's|#define JPEG_SUPPORT 1|/* #define JPEG_SUPPORT 1 */|g' libtiff/include-SPM/tiffconf.h
sed -i '' -e 's|#define ZSTD_SUPPORT 1|/* #define ZSTD_SUPPORT 1 */|g' libtiff/include-SPM/tif_config.h
sed -i '' -e 's|#define WEBP_SUPPORT 1|/* #define WEBP_SUPPORT 1 */|g' libtiff/include-SPM/tif_config.h
sed -i '' -e 's|#define LZMA_SUPPORT 1|/* #define LZMA_SUPPORT 1 */|g' libtiff/include-SPM/tif_config.h

xcodebuild -scheme LibTIFF -destination generic/platform=iOS
xcodebuild -scheme LibTIFF -destination generic/platform=macOS
xcodebuild -scheme LibTIFF -destination generic/platform=watchOS
xcodebuild -scheme LibTIFF -destination generic/platform=tvOS
xcodebuild -scheme LibTIFF -destination generic/platform=visionOS

echo "Update Successful!"