// swift-tools-version: 5.7
import PackageDescription

let package = Package(
    name: "LibTIFF",
    products: [
        .library(name: "LibTIFF", targets: ["LibTIFF"])
    ],
    dependencies: [],
    targets: [
        .target(
            name: "LibTIFF",
            dependencies: [],
            path: "libtiff",
            exclude: [
                "tif_win32.c",
                "mkg3states.c",
                "mkspans.c",
            ],
            publicHeadersPath: "include-SPM"
        )
    ],
    cxxLanguageStandard: .cxx11
)
